Consul cluster deployment
=========================

(Build and deployment for development with local deployment)

Build the Terraform configuration (assuming everything else is built):

    mvn -P cloud-qemu clean package

Deploy (`cluster_size` must be an odd number):

    cd target/terraform
    terraform init
    terraform apply -var cluster_size=3
    sh bootstrap.sh
    cd -

Cleanup:

    cd target/terraform
    terraform destroy
    rm acl_master_token
    cd -
