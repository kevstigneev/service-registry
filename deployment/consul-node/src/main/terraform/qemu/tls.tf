#
# TLS configuration for Consul server.
# It is a prototype - a Registry TLS configuration must be shared by Consul and
# Vault.
#

#
# Outputs here for easier removal
#

output "ca_cert" {
    description = "Bootstrapping CA certificate"
    value       = "${tls_self_signed_cert.ca.cert_pem}"
}

#
# CA
#

resource "tls_private_key" "ca" {
    algorithm   = "RSA"
}

resource "tls_self_signed_cert" "ca" {
    key_algorithm         = "${tls_private_key.ca.algorithm}"
    private_key_pem       = "${tls_private_key.ca.private_key_pem}"

    # Valid for a year (8760 hours)
    validity_period_hours = 8760

    is_ca_certificate     = "true"
    allowed_uses = [
        "cert_signing",
    ]
    subject {
        common_name         = "Consul CA"
        organizational_unit = "${data.null_data_source.config.outputs["env"]}"
        organization        = "@project.groupId@"
    }
}

#
# Server keys and certificates
#

resource "tls_private_key" "server" {
    count       = "${var.cluster_size}"

    algorithm   = "RSA"
}

resource "tls_cert_request" "server" {
    count           = "${var.cluster_size}"

    key_algorithm   = "${tls_private_key.server.*.algorithm[count.index]}"
    private_key_pem = "${tls_private_key.server.*.private_key_pem[count.index]}"

    ip_addresses = [
        "${element(libvirt_domain.node.*.network_interface.0.addresses[count.index], 0)}"
    ]
    dns_names = [
        # Required or Consul server authentication
        "server.${var.datacenter}.${var.domain}",
        "${libvirt_domain.node.*.name[count.index]}",
    ]
    subject {
        common_name = "${libvirt_domain.node.*.name[count.index]}"
    }
}

resource "tls_locally_signed_cert" "server" {
    count                 = "${var.cluster_size}"

    cert_request_pem      = "${tls_cert_request.server.*.cert_request_pem[count.index]}"
    ca_key_algorithm      = "${tls_private_key.ca.algorithm}"
    ca_private_key_pem    = "${tls_private_key.ca.private_key_pem}"
    ca_cert_pem           = "${tls_self_signed_cert.ca.cert_pem}"

    # Valid for 2 months (1440 hours)
    validity_period_hours = 1440

    # This set works for Consul RPC TLS. Possibly it's surplus for the purpose.
    allowed_uses = [
        "key_encipherment",
        "key_agreement",
        "digital_signature",
        "server_auth",
        "client_auth",
    ]
}

# Consul gossip encryption key configuration.
# This key is for provisioning only. The cluster bootstrapping script replaces it.
# So this key becomes useless and safe to leave in the Terraform state.
#
data "template_file" "consul_encryption_config" {
    template = "${file("${path.module}/templates/agent-encryption.json")}"
    vars     = {
        consul_encryption_key = "${random_id.consul_encryption_key.b64_std}"
    }
}
resource "random_id" "consul_encryption_key" {
    byte_length = 32
}

#
# Certificate and gossip encryption key provisioning to the running VMs
#

# Certificates include VM addresses so they cannot be issued in advance.
# Consul requires server key and certificate, so its launch is deferred.
#
resource "null_resource" "bootstrap" {
    count = "${var.cluster_size}"

    connection {
        host        = "${self.triggers.address}"
        user        = "${data.null_data_source.terraform.outputs["ssh_user"]}"
        private_key = "${data.null_data_source.terraform.outputs["ssh_priv_key"]}"
    }

    # Provision server certificate and gossip encryption configuration
    provisioner "file" {
        content     = "${self.triggers.cert}${tls_self_signed_cert.ca.cert_pem}"
        destination = "/tmp/server_cert.pem"
    }
    provisioner "file" {
        content     = "${self.triggers.key}"
        destination = "/tmp/server_key.pem"
    }
    provisioner "file" {
        content     = "${self.triggers.enc_cfg}"
        destination = "/tmp/agent-encryption.json"
    }
    provisioner "remote-exec" {
        inline = [
            "sudo chmod 0600 /tmp/server_key.pem /tmp/agent-encryption.json",
            "sudo chown consul:consul /tmp/server_cert.pem /tmp/server_key.pem /tmp/agent-encryption.json",
            "sudo mv -f /tmp/server_key.pem '${data.template_file.consul_tls_config.vars.key_file}'",
            "sudo mv -f /tmp/server_cert.pem '${data.template_file.consul_tls_config.vars.cert_file}'",
            "sudo mv -f /tmp/agent-encryption.json '${data.ignition_directory.consul_config.path}'",
        ]
    }

    # Deferred start of Consul service. Or restart to pick updated certificates
    provisioner "remote-exec" {
        inline = [
            "sudo systemctl -q enable ${data.ignition_systemd_unit.consul.name}",
            "sudo systemctl -q restart ${data.ignition_systemd_unit.consul.name}",
        ]
    }

    triggers {
        id      = "${libvirt_domain.node.*.id[count.index]}"
        address = "${element(libvirt_domain.node.*.network_interface.0.addresses[count.index], 0)}"
        cert    = "${tls_locally_signed_cert.server.*.cert_pem[count.index]}"
        key     = "${tls_private_key.server.*.private_key_pem[count.index]}"
        enc_cfg = "${data.template_file.consul_encryption_config.rendered}"
    }
}
