#
# Terraform configuration for service registry VMs.
#

# The shared base image for all VMs
#
resource "libvirt_volume" "registry_image" {
    name            = "${data.null_data_source.config.outputs["env"]}-registry-image.qcow2"
    source          = "${data.null_data_source.config.outputs["image_source"]}"
}

# It's VM name holder.
#
data "null_data_source" "node" {
    count  = "${var.cluster_size}"

    inputs = {
        name = "${data.null_data_source.config.outputs["env"]}-consul-${count.index}"
    }
}

# The root file system disks.
#
resource "libvirt_volume" "root" {
    count           = "${var.cluster_size}"

    name            = "${lookup(data.null_data_source.node.*.outputs[count.index], "name")}.qcow2"
    base_volume_id  = "${libvirt_volume.registry_image.id}"
}

# Startup configurations for CoreOS VMs
#
resource "libvirt_ignition" "node" {
    count           = "${var.cluster_size}"

    name            = "${lookup(data.null_data_source.node.*.outputs[count.index], "name")}-ignition"
    content         = "${data.ignition_config.consul.*.rendered[count.index]}"
}

# The VMs.
#
resource "libvirt_domain" "node" {
    count           = "${var.cluster_size}"

    name            = "${lookup(data.null_data_source.node.*.outputs[count.index], "name")}"
    memory          = "512"
    vcpu            = "1"
    coreos_ignition = "${libvirt_ignition.node.*.id[count.index]}"
    disk {
        volume_id       = "${libvirt_volume.root.*.id[count.index]}"
    }
    network_interface {
        network_name    = "default"
        wait_for_lease  = "true"
    }
    # Without this block cloud-init partially fails
    console {
        type            = "pty"
        target_port     = "0"
        target_type     = "serial"
    }
}
