#
# Ignition configuration for CoreOS VMs.
#

# The Ignition configuration object for VM provisioning.
# Aggregates specific configuration resources below.
#
data "ignition_config" "consul" {
    count           = "${var.cluster_size}"

    users = [
        "${data.ignition_user.core.rendered}",
        "${data.ignition_user.consul.rendered}",
    ]
    groups = [
        "${data.ignition_group.consul.rendered}",
    ]
    directories = [
        "${data.ignition_directory.consul_data.rendered}",
        "${data.ignition_directory.consul_config.rendered}",
    ]
    files = [
        "${data.ignition_file.hostname.*.rendered[count.index]}",
        "${data.ignition_file.consul_server_config.*.rendered[count.index]}",
        "${data.ignition_file.consul_network_config.rendered}",
        "${data.ignition_file.consul_security_config.rendered}",
        "${data.ignition_file.consul_tls_config.rendered}",
        "${data.ignition_file.bootstrap_ca_cert.rendered}",
    ]
    links = [
        "${data.ignition_link.bootstrap_ca_cert.rendered}",
    ]
    systemd = [
        "${data.ignition_systemd_unit.update-engine.rendered}",
        "${data.ignition_systemd_unit.locksmithd.rendered}",
        "${data.ignition_systemd_unit.consul.rendered}",
    ]
}

#
# Common system configuration.
#

data "ignition_user" "core" {
    name = "${data.null_data_source.config.outputs["ssh_user"]}"
    ssh_authorized_keys = [
        "${data.null_data_source.terraform.outputs["ssh_pub_key"]}",
        "${data.null_data_source.config.outputs["ssh_user_key"]}",
    ]
}

# Configure the specific hostname
#
data "ignition_file" "hostname" {
    count           = "${var.cluster_size}"

    filesystem = "root"          # default `ROOT` filesystem
    path       = "/etc/hostname"
    mode       = 420             # decimal 0644
    content {
        content = "${lookup(data.null_data_source.node.*.outputs[count.index], "name")}"
    }
}

# Disable CoreOS auto-update (https://coreos.com/os/docs/latest/update-strategies.html)
#
data "ignition_systemd_unit" "update-engine" {
    name = "update-engine.service"
    mask = "true"
}
data "ignition_systemd_unit" "locksmithd" {
    name = "locksmithd.service"
    mask = "true"
}

#
# Set up Consul service
#

# Consul user owns files and runs the Consul container
#
data "ignition_user" "consul" {
    name            = "consul"
    primary_group   = "${data.ignition_group.consul.gid}"
    no_user_group   = "true"
    home_dir        = "/var/lib/consul"
    no_create_home  = "true"
    shell           = "/bin/nologin"
    system          = "true"
    uid             = 799
}
data "ignition_group" "consul" {
    name = "consul"
    gid  = 799
}

# Consul persistent data and configuration on disk
#
data "ignition_directory" "consul_data" {
    filesystem  = "root"
    path        = "/var/lib/consul"
    mode        = "0700"
    uid         = "${data.ignition_user.consul.uid}"
    gid         = "${data.ignition_group.consul.gid}"
}
data "ignition_directory" "consul_config" {
    filesystem  = "root"
    path        = "/etc/consul"
    mode        = "0755"
}

# SystemD unit to start Consul service on boot
# It is disabled at start. Enable it later when TLS certificates are provisioned.
#
data "ignition_systemd_unit" "consul" {
    name    = "consul.service"
    content = "${data.template_file.systemd_consul_service.rendered}"
    enabled = "false"
}
data "template_file" "systemd_consul_service" {
    template = "${file("${path.module}/templates/consul.service")}"
    vars     = {
        uid             = "${data.ignition_user.consul.uid}"
        gid             = "${data.ignition_group.consul.gid}"
        data_dir        = "${data.ignition_directory.consul_data.path}"
        config_dir      = "${data.ignition_directory.consul_config.path}"
        container_image = "${data.null_data_source.config.outputs["consul_image"]}"
    }
}

#
# Consul server configuration
#

# Server-specific configuration; boostrapping too
#
data "ignition_file" "consul_server_config" {
    count      = "${var.cluster_size}"

    filesystem = "root"
    path       = "${data.ignition_directory.consul_config.path}/agent-server.json"
    mode       = "0644"
    content {
        content = "${data.template_file.consul_server_config.*.rendered[count.index]}"
    }
}
data "template_file" "consul_server_config" {
    count    = "${var.cluster_size}"

    template = "${file("${path.module}/templates/agent-server.json")}"
    vars     = {
        consul_bootstrap    = "${count.index == 0 ? "true" : "false"}"
        log_level           = "${var.log_level}"
    }
}

# Configure network addresses, ports and names
#
data "ignition_file" "consul_network_config" {
    filesystem = "root"
    path       = "${data.ignition_directory.consul_config.path}/agent-network.json"
    mode       = "0644"
    content {
        content = "${data.template_file.consul_network_config.*.rendered[count.index]}"
    }
}
data "template_file" "consul_network_config" {
    template = "${file("${path.module}/templates/agent-network.json")}"
    vars     = {
        consul_datacenter   = "${var.datacenter}"
        consul_domain       = "${var.domain}"
        consul_dns_port     = "8600"
        consul_http_port    = "8500"
        consul_https_port   = "8501"
    }
}

# Configure Consul ACL system
#
data "ignition_file" "consul_security_config" {
    filesystem = "root"
    path       = "${data.ignition_directory.consul_config.path}/agent-security.json"
    mode       = "0600"
    uid        = "${data.ignition_user.consul.uid}"
    gid        = "${data.ignition_group.consul.gid}"
    content {
        content = "${data.template_file.consul_security_config.rendered}"
    }
}
data "template_file" "consul_security_config" {
    template = "${file("${path.module}/templates/agent-security.json")}"
    vars     = {
        consul_datacenter   = "${var.datacenter}"
    }
}

# Consul TLS
#
data "ignition_file" "consul_tls_config" {
    filesystem = "root"
    path       = "${data.ignition_directory.consul_config.path}/agent-tls.json"
    mode       = "0644"
    content {
        content = "${data.template_file.consul_tls_config.rendered}"
    }
}
data "template_file" "consul_tls_config" {
    template = "${file("${path.module}/templates/agent-tls.json")}"
    vars     = {
        ca_file   = "${data.ignition_file.bootstrap_ca_cert.path}"
        cert_file = "${data.ignition_directory.consul_config.path}/server_cert.pem"
        key_file  = "${data.ignition_directory.consul_config.path}/server_key.pem"
    }
}

# The bootstrapping CA certificate authorizing Registry clients and servers
#
data "ignition_file" "bootstrap_ca_cert" {
    filesystem = "root"
    path       = "${data.ignition_directory.consul_config.path}/ca-bootstrap.pem"
    mode       = "0644"
    content {
        content = "${tls_self_signed_cert.ca.cert_pem}"
    }
}

# Add it to system trusted certs - just for convenience
# (<https://coreos.com/os/docs/latest/adding-certificate-authorities.html>).
# FIXME: It needs to run `update-ca-certificates` in addition to that.
#
data "ignition_link" "bootstrap_ca_cert" {
    filesystem = "root"
    path       = "/etc/ssl/certs/ca-bootstrap.pem"
    target     = "${data.ignition_file.bootstrap_ca_cert.path}"
}
