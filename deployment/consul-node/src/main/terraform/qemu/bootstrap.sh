#!/bin/sh
#
# Bootstap Consul cluster.
# To run in the Terraform configuration directory.
#

set -eu

just_curl() {
    if [ "${CACERT:-}" ]; then
        set -- --cacert ${CACERT} "$@"
    fi
    curl --fail --silent --show-error --retry 20 --retry-delay 3 "$@"
}

consul_curl() {
    addr="$1"
    shift
    path="$1"
    shift
    if [ "${CONSUL_HTTP_TOKEN:-}" ]; then
        set -- --header "X-Consul-Token: ${CONSUL_HTTP_TOKEN}" "$@"
    fi
    just_curl "$@" "https://${addr}:${CONSUL_HTTPS_PORT}/v1${path}"
}

escape() {
    echo "$@" | sed 's/"/\\\0/g'
}

log() {
    echo "$@" >&2
}

tmpfiles=""

# Need jq
jq --version >/dev/null

# Need `addresses` output from Terraform deployment
log "Check if it runs in the correct directory and Terraform state is available"
terraform output addresses >/dev/null

# "Environment" name
env=$(terraform output env)

# Bootstrapping CA certificate
CACERT=$(mktemp)
tmpfiles="${tmpfiles} ${CACERT}"
terraform output ca_cert >${CACERT}

# Collect Consul nodes from Terraform outputs
nodes=""
for i in $(seq 0 5); do
    node=$(terraform output -json addresses | jq -r ".value[${i}][0]")
    [ "${node}" != "null" ] || break
    nodes="${nodes}${nodes:+ }${node}"
done
log "Consul nodes: ${nodes}"
[ -n "${nodes}" ]

CONSUL_HTTPS_PORT=$(terraform output consul_https_port)

# Bootstrap ACL at the 1st node - it should be a self-proclaimed cluster
node0=$(echo ${nodes} | cut -d' ' -f1)
log "Bootstrap ACL at ${node0}"
acl_master_token=$(consul_curl ${node0} /acl/bootstrap -X PUT | jq -r .ID)
if [ -z "${acl_master_token}" ]; then
    log "Apparently ACL is already bootstrapped, look for saved acl_master_token"
    # DEBUG
    acl_master_token=$(cat acl_master_token)
else
    # DEBUG
    log "DEBUG WARNING persisting acl_master_token on disk"
    (umask 0066; echo "${acl_master_token}" >acl_master_token)
fi
[ -n "${acl_master_token}" ]
echo "acl_master_token=${acl_master_token}"

export CONSUL_HTTP_TOKEN=${acl_master_token}

# Join nodes to the cluster and configure ACL replication
for node in ${nodes}; do
    if [ "${node}" != "${node0}" ]; then
        log "Join ${node} to the cluster"
        consul_curl ${node0} /agent/join/${node} -X PUT
        # Consul needs some time to join before calling it, otherwise it responds with 403
        sleep 1
    fi

    node_name=$(consul_curl ${node} /agent/self | jq -r .Config.NodeName)
    [ -n "${node_name}" ]

    log "Provision Agent Token for node ${node_name} at ${node}"
    acl_rules='node "'${node_name}'" {policy = "write"} service "" {policy = "read"}'
    req_data='{"Name":"Agent Token for '${node_name}'", "Type":"client", "Rules":"'$(escape "${acl_rules}")'"}'
    agent_token=$(consul_curl ${node0} /acl/create -X PUT -d "${req_data}" | jq -r .ID)
    [ -n "${agent_token}" ]

    log "Set Agent Token for node ${node_name} at ${node}"
    consul_curl ${node} /agent/token/acl_agent_token -X PUT -d '{"Token":"'${agent_token}'"}'
done

# Set anonymous policy
log "Update Anonymous ACL"

# Find if the anonymous policy already exists and create it otherwise
policy_name="anonymous"
policy_id=$(consul_curl ${node0} /acl/policies \
        | jq -r --arg name "${policy_name}" '.[]|select(.Name == $name).ID')
if [ -n "${policy_id}" ]; then
    log "ACL policy '${policy_name}' already exists, update it"
else
    log "Create an ACL policy to see Consul service and nodes"
fi
acl_rules='service "consul" {policy = "read"} node_prefix "'${env}'-consul-" {policy = "read"}'
req_data='{"Name":"'${policy_name}'", "Description":"", "Rules":"'$(escape "${acl_rules}")'"}'
policy_id=$(consul_curl ${node0} /acl/policy${policy_id:+/${policy_id}} -X PUT -d "${req_data}" \
        | jq -r .ID)
[ -n "${policy_id}" ]

# Anonymous access token is built-in (https://www.consul.io/docs/acl/acl-system.html#builtin-tokens)
log "Update anonymous token ACL policies"
req_data='{"SecretID":"anonymous", "Policies":[{"ID":"'${policy_id}'"}]}'
consul_curl ${node0} /acl/token/00000000-0000-0000-0000-000000000002 -X PUT -d "${req_data}" >/dev/null

# Rotate Gossip encryption key and retire the provisioning one from the Terraform state
# (https://www.consul.io/api-docs/operator/keyring)
log "Rotate Gossip encryption key"

old_gossip_keys=$(consul_curl ${node0} /operator/keyring \
    | jq -r '.[]|select(.WAN|not).Keys|keys|join(" ")')
log "The old gossip key is $(echo ${old_gossip_keys} | cut -f1 -d' ')"

gossip_key=$(dd if=/dev/urandom bs=32 count=1 status=none | base64)
req_data='{"Key":"'${gossip_key}'"}'
log "Add the new gossip key ${gossip_key}"
consul_curl ${node0} /operator/keyring -X POST -d "${req_data}"
log "Change primary gossip key"
consul_curl ${node0} /operator/keyring -X PUT -d "${req_data}"

# Wait till the number of nodes with the new key reaches the total number of nodes
log "Wait till the new gossip key propagates"
while [ $(consul_curl ${node0} /operator/keyring \
            | jq --arg key "${gossip_key}" '.[] | select(.WAN|not) | (.Keys[$key] < .NumNodes)' \
        ) = "true" ]; do
    sleep 3
done

log "Remove $(echo ${old_gossip_keys} | wc -w) old gossip key(s)"
for key in ${old_gossip_keys}; do
    consul_curl ${node0} /operator/keyring -X DELETE -d '{"Key":"'${key}'"}'
done

rm -f ${tmpfiles}
