#
# Interface variables for the Terraform module.
#

variable "libvirt_uri" {
    description = "Libvirt URI"
    type        = "string"
    default     = "qemu:///system"
}

variable "env" {
    description = "A distinctive name for the set of infrastructure resources"
    type        = "string"
    default     = ""
}

variable "cluster_size" {
    description = "Number of nodes in the cluster"
    type        = "string"
    default     = "1"
}

variable "datacenter" {
    description = "Consul Datacenter"
    type        = "string"
    default     = "dc0"
}

variable "domain" {
    description = "DNS domain for Consul DNS API and TLS certificates"
    type        = "string"
    default     = "consul."
}

variable "log_level" {
    description = "Logging verbosity (one of 'trace', 'debug', 'info', 'warn', and 'err')"
    type        = "string"
    default     = "info"
}

#
# Specific to local development environment (cloud-qemu)
#

variable "ssh_key" {
    description = "Operator's public SSH key in OpenSSH format"
    type        = "string"
    default     = ""
}
variable "ssh_key_file" {
    description = "Operator's public OpenSSH key file (relative to home directory)"
    type        = "string"
    default     = ".ssh/id_rsa.pub"
}
