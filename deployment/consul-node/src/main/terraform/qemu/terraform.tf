#
# Utility resources for Terraform configuration
#

data "null_data_source" "terraform" {
    inputs = {
        ssh_user     = "${data.null_data_source.config.outputs["ssh_user"]}"
        ssh_pub_key  = "${tls_private_key.terraform_ssh.public_key_openssh}"
        ssh_priv_key = "${tls_private_key.terraform_ssh.private_key_pem}"
    }
}

resource "tls_private_key" "terraform_ssh" {
    algorithm = "RSA"
}
