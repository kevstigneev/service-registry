#
# Outputs of the Terraform module.
#

output "addresses" {
    description = "IP addresses of VMs (list of lists)"
    value       = ["${libvirt_domain.node.*.network_interface.0.addresses}"]
}
output "ssh_username" {
    description = "Name of the default SSH user"
    value       = "${data.null_data_source.config.outputs["ssh_user"]}"
}
output "env" {
    description = "A distinctive name for the set of infrastructure resources"
    value       = "${data.null_data_source.config.outputs["env"]}"
}
output "consul_https_port" {
    description = "TCP port for Consul HTTPS API"
    value       = "${data.template_file.consul_network_config.vars.consul_https_port}"
}
