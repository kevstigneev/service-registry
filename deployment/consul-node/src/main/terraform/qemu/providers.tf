#
# Terraform providers.
#

# Libvirt provider <https://github.com/dmacvicar/terraform-provider-libvirt>.
# Works with local QEMU/KVM.
# It respects environment variables:
# - LIBVIRT_DEFAULT_URI - libvirt URI, default for `uri` parameter.
#
provider "libvirt" {
    version = "~> 0.5"

    uri = "${var.libvirt_uri}"
}

provider "ignition" {version = "~> 1.2"}

provider "external" {version = "~> 1.2"}
provider "null" {version = "~> 2.1"}
provider "random" {version = "~> 2.2"}
provider "template" {version = "~> 2.1"}
provider "tls" {version = "~> 2.1"}

# Configuration parameters with computed defaults
# TODO: `ssh_user` should come from the image artifact.
#
data "null_data_source" "config" {
    inputs = {
        env             = "${coalesce(var.env, random_pet.env.id)}"
        image_source    = "@org.bitbucket.kevstigneev.service-registry-h:registry-image:qcow2:qemu-amd64@"
        consul_image    = "@org.bitbucket.kevstigneev.service-registry-h.consul-container.imageName@"
        ssh_user        = "core"
        ssh_user_key    = "${chomp(coalesce(var.ssh_key,
                                            file("${data.external.env.result.HOME}/${var.ssh_key_file}")))}"
    }
}
resource "random_pet" "env" {
    length = 1
}

# A subset of environment variables
#
data "external" "env" {
    program = ["sh", "${path.module}/data_env.sh"]
}
