Terafform configuration for CoreOS at QEMU
==========================================

It uses CoreOS Ignition VM configuration method. The more generic cloud-config
method does not work because terraform-libvirt uses
[NoCloud](https://cloudinit.readthedocs.io/en/latest/topics/datasources/nocloud.html)
provisioning datacource while CoreOS supports only
[Config Drive](https://cloudinit.readthedocs.io/en/latest/topics/datasources/configdrive.html).
