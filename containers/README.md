Container images
----------------

This group of Maven modules packages service registry software to Docker
container images. Then these container images are installed to the registry VM
image. Later the containers may be directly run at Kubernetes.

Image TAR files are the build artifacts. One can load it to Docker by

    docker load --input target/jib-image.tar

Image build tool is [Google Jib](https://github.com/GoogleContainerTools/jib).
It does not use Docker daemon or CLI.

The base Docker image is the "base" image from
[Google distroless](https://github.com/GoogleContainerTools/distroless). These
images do not have shell or any CLI tools and may run statically build native
executable binaries.


### Maintenance

Update base container images.

Update: The image POM file, `baseImageId` property:

    <properties>
        <!-- Base container image by 2020-02-20 -->
        <baseImageName>gcr.io/distroless/static</baseImageName>
        <baseImageId>sha256:c6d5981545ce1406d33e61434c61e9452dad93ecd8397c41e89036ef977a88f4</baseImageId>
        ...
    </properties>

Find the last images:

- [`gcr.io/distroless/static`](https://gcr.io/distroless/static)

Note: <https://github.com/GoogleContainerTools/distroless> promotes
`distroless/static-debian10` rather than `distroless/static`. Not sure what is
the difference and should I switch from `static` to `static-debian10`.
[`GoogleContainerTools/distroless/base/`](https://github.com/GoogleContainerTools/distroless/tree/master/base)
still refers to `static`, these recent images are still available.


### TODO

- Remove JAR content from the container image.
  By default JIB includes the module JAR content. Here the module JAR is just
  for dependency management, it's irrelevant to the executable artifact (image).
