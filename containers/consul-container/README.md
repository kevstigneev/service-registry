Consul container image
----------------------

### Build the image

Build the image and optionally load it to the Docker:

    mvn package
    docker load -i target/jib-image.tar 


### Run the container

Host prerequisites:

- Unix user and group with fixed UID and GID (technically optional but recommended):

        groupadd -g {GID} consul
        useradd -d / -M -g consul -N -s /bin/nologin -u {UID} consul

- Directories for data and configuration:

        mkdir {DATA_HOST_PATH} && chown {UID} {DATA_HOST_PATH} && chmod 700 {DATA_HOST_PATH}
        mkdir {CONFIG_HOST_PATH}

Launch Consul server (Consul executable is the container entry point):

    docker run -d --rm --name=consul \
        --network=host \
        --user={UID}:{GID} \
        --ulimit=nofile:1048576 \
        --volume={DATA_HOST_PATH}:/var/lib/consul \
        --volume={CONFIG_HOST_PATH}:/etc/consul:ro \
        org.bitbucket.kevstigneev.service-registry-h/consul-container:0.1.0-SNAPSHOT \
        agent -server -ui \
            -data-dir=/var/lib/consul \
            -config-dir=/etc/consul \
            -config-file=/etc/consul-server.json

Consul container ships default configuration file for a Consul server:
`/etc/consul-server.json` (`src/main/jib/etc/consul-server.json` in the source).

Container limit to the number of open files should accommodate incoming
connections. A lot of them if clients are consul-templates.

Stop Consul server:

    docker stop --time=20 consul


### Maintenance

Update the base image as explained [above](../README.md).

Update Consul agent executable binary.

1. Update Consul version in project dependencies
    ([`dependencies/consul`](../../dependencies/consul/) in the repository root).
    In `pom.xml` file update the POM version to the new Consul version. It will
    be downloaded from the Consul web site.

        <project ...>
            <modelVersion>4.0.0</modelVersion>
            <groupId>io.consul</groupId>
            <artifactId>consul</artifactId>
            <version>1.5.3</version>                <!-- Update this version -->
            ...

    Then install the new Consul version to the project dependencies. In the same folder:

        make install

2. Update this `pom.xml` file, `<dependencies>` section. The new version must
    match the version in the previous step.

        <dependencies>
            ...
            <dependency>
                <groupId>io.consul</groupId>
                <artifactId>consul</artifactId>
                <version>1.5.3</version>            <!-- Update this version -->
                ...
            </dependency>
            ...
        </dependencies>
