#
# Download Hashicorp binaries for the build.
# The Makefile should not be called directly but rather included by concrete
# makefiles that in turn called by Maven like that:
#     mvn install
#
# Required Make variables:
# - PRODUCT - the Hashicorp product to download;
# - VERSION - version of the binary package to download;
# - ARCH - target architecture (such as linux_amd64);
# - TARGET_DIR - where to put the downloaded files.
#

DOWNLOAD_URL = https://releases.hashicorp.com
DOWNLOAD = curl --fail --silent --show-error --location --remote-name

parameters:
	@: "$${PRODUCT:?Variable is not defined}"
	@: "$${VERSION:?Variable is not defined}"
	@: "$${ARCH:?Variable is not defined}"
	@: "$${TARGET_DIR:?Variable is not defined}"

$(TARGET_DIR): | parameters
	mkdir -p $@

$(TARGET_DIR)/$(PRODUCT)_$(VERSION)_SHA256SUMS.sig: | $(TARGET_DIR) parameters
	cd $(@D) && $(DOWNLOAD) "$(DOWNLOAD_URL)/$(PRODUCT)/$(VERSION)/$(@F)"

$(TARGET_DIR)/$(PRODUCT)_$(VERSION)_SHA256SUMS: $(TARGET_DIR)/$(PRODUCT)_$(VERSION)_SHA256SUMS.sig | $(TARGET_DIR) parameters
	cd $(@D) && $(DOWNLOAD) "$(DOWNLOAD_URL)/$(PRODUCT)/$(VERSION)/$(@F)"
	# Verify Hashicorp signature.
	# If it fails due to unknown key, install it from https://www.hashicorp.com/security.html
	gpg --verify $< $@

$(TARGET_DIR)/$(PRODUCT)_$(VERSION)_$(ARCH).zip: $(TARGET_DIR)/$(PRODUCT)_$(VERSION)_SHA256SUMS | $(TARGET_DIR) parameters
	cd $(@D) && $(DOWNLOAD) "$(DOWNLOAD_URL)/$(PRODUCT)/$(VERSION)/$(@F)"
	cd $(<D) && sha256sum -c --ignore-missing $(<F)

$(TARGET_DIR)/$(PRODUCT): $(TARGET_DIR)/$(PRODUCT)_$(VERSION)_$(ARCH).zip | $(TARGET_DIR) parameters
	unzip -o -DD $< $(@F) -d $(@D)

$(TARGET_DIR)/$(PRODUCT)-$(VERSION)-$(ARCH).tar.gz: $(TARGET_DIR)/$(PRODUCT)_$(VERSION)_$(ARCH).zip | $(TARGET_DIR) parameters
	unzip -o $< -d $(@D)/$(basename $(<F))
	tar -czvvf $@ --owner=hashicorp --group=hashicorp --mode=g-w,o-w -C $(@D)/$(basename $(<F)) .
	-rm -rf $(@D)/$(basename $(<F))

.PHONY: parameters

.DELETE_ON_ERROR:
