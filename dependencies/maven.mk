#
# Defer regular build actions to Maven
#

MVN = mvn
MVN_ARGS = -C -B

MAVEN_PHASES = \
    clean \
    validate \
    compile \
    test \
    package \
    verify \
    install \
    deploy

missing-target:
	$(error The makefile requires an explicit target)

$(MAVEN_PHASES): pom.xml
	$(MVN) $(MVN_ARGS) $@

.PHONY: missing-target $(MAVEN_PHASES)
