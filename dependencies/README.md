Build time dependencies
=======================

Install build time dependencies as Maven artifacts. It should be done just once
at a local system. They also may be deployed to a remote Maven repository and
shared that way within the team, so others don't need to install these
artifacts themselves.

Software packages for installation to the application VM are re-packaged as TAR
archives.
