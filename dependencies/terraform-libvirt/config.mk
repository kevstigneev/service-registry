#
# Common parameters.
#

PRODUCT = terraform-provider-libvirt
# The last release for Terraform 0.11.x
VERSION = 0.5.2
DOWNLOAD_URL = https://github.com/dmacvicar/$(PRODUCT)
TF_PLUGIN_DIR = $(HOME)/.terraform.d/plugins

DOWNLOAD = curl --fail --silent --show-error --location --remote-name
