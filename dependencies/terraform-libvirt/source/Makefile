#
# Build Terraform libvirt provider from sources.
# Requires:
# - golang
# - golint
# - libvirt-dev
#

include ../config.mk

BUILD_DIR = target

help:
	# Makefile targets:
	#   install - installs $(PRODUCT) for the current user.
	#   clean   - removes the working files.

$(BUILD_DIR) $(TF_PLUGIN_DIR):
	mkdir -p $@

$(BUILD_DIR)/v$(VERSION).tar.gz: | $(BUILD_DIR)
	cd $(@D) && $(DOWNLOAD) "$(DOWNLOAD_URL)/archive/$(@F)"

gopath = $(abspath $(BUILD_DIR))
source_dir := $(gopath)/src/$(patsubst https://%,%,$(DOWNLOAD_URL))

$(source_dir): $(BUILD_DIR)/v$(VERSION).tar.gz
	mkdir -p $@
	tar -xzf $< --strip-components=1 -C $@
	test -f $@/Makefile

$(source_dir)/$(PRODUCT): $(source_dir)
	# As of 0.5.2 the test coverage is broken, thus have to skip tests
	$(MAKE) -C $< GO111MODULE=on GOFLAGS=-mod=vendor CGO_ENABLED=1 build

build: $(source_dir)/$(PRODUCT)

install: $(source_dir)/$(PRODUCT) | $(TF_PLUGIN_DIR)
	install -m 755 -p $< $(firstword $|)/$(PRODUCT)_v$(VERSION)

clean:
	-rm -rf $(BUILD_DIR)

.PHONY: clean build install

.DELETE_ON_ERROR:
