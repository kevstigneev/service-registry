Flatcar Container Linux image
=============================

[Flatcar Container Linux](https://www.flatcar-linux.org/) is an immutable Linux
distribution for containers. It is a friendly fork of CoreOS Container Linux
and as such, compatible with it.

This module downloads a Flatcar Linux VM image for local use with QEMU/libvirt
and stores it in the local Maven repository. It uses Maven under the hood but
the entry point is the Makefile.

How to use it
-------------

Install the Flatcar Linux siging GPG key (once for the first time):

    make install-key

Download and install the image:

    make install

How to update Flatcar Linux version
-----------------------------------

The POM file (`pom.xml`) configures the Flatcar Linux version and release
channel:

- Version is equal to the POM version.
- Release channel is in `releaseChannel` property.
