Service registry using HashiCorp Vault and Consul
=================================================

Service registry service (sic) for Google Cloud Compute Engine (GCE) and
possibly Kubernetes Engine (GKE) using HashiCorp
[Consul](https://www.consul.io/) and [Vault](https://www.vaultproject.io/).


Build
-----

### Prerequisites

Supported platforms: Linux (amd64), should be adaptable to other unix-like
systems such as Mac OS.

Required tools:

- GNU Make.
- Apache Maven 3.5.0+.
- HashiCorp [Terraform][terraform] 0.11.x (0.12.x and higher need a review).

Public PGP keys should be installed to the local GPG keyring:

- HashiCorp. Install it from <https://www.hashicorp.com/security.html>.

### Steps

1. `(cd dependencies && make)` - install build time dependencies.
2. Install `bitbucket.org/kevstigneev/maven-parent`.


Local development
-----------------

It can be built and run locally without access to a public cloud. The only
supported platform is Linux(amd64). It uses QEMU/KVM as the virtualization
runtime and [libvirt][] as the API to it.

Additional prerequisites:

- [Libvirt][libvirt] and QEMU/KVM.
  [Installation guide for Ubuntu](https://help.ubuntu.com/community/KVM/Installation),
  at other distributions it's straightforward as well.
- If [Terraform provider for libvirt][terraform-provider-libvirt] does not ship
  a binary package for the local platform:
    - [Go](https://golang.org/) 1.11+ development environment (golang, golint).
    - libvirt development libraries (libvirt-dev) 1.2.14 or newer.

Public PGP keys should be installed to the local GPG keyring:

- FlatCar Linux. Install it from <https://www.flatcar-linux.org/security/image-signing-key/>.

Build steps:

1. `(cd dependencies && make qemu)` - install build time dependencies.
2. `mvn install -P cloud-qemu` - build VM image.

Build dependencies include Flatcar Linux VM image so it takes a while to download.

### Local deployment with Terraform

It uses 3rd party [Terraform provider for libvirt][terraform-provider-libvirt].

### Known issues

On Ubuntu distros SELinux is enforced by qemu even if it is disabled globally,
this might cause unexpected
`Could not open '/var/lib/libvirt/images/<FILE_NAME>': Permission denied`
errors. Double check that `security_driver = "none"` is uncommented in
`/etc/libvirt/qemu.conf` and issue `sudo systemctl restart libvirt-bin` to
restart the daemon.
([terraform-provider-libvirt][] [#546](https://github.com/dmacvicar/terraform-provider-libvirt/issues/546#issuecomment-467151566))

[libvirt]: https://libvirt.org/
[terraform]: https://www.terraform.io/
[terraform-provider-libvirt]: https://github.com/dmacvicar/terraform-provider-libvirt
