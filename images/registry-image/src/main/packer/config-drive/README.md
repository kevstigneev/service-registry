Cloud config drive
==================

It is a template for an [OpenStack config drive](https://docs.openstack.org/nova/stein/user/config-drive.html).
Config drive is a [method to pass cloud-config](https://coreos.com/os/docs/latest/config-drive.html)
to a VM. Here it configures SSH keys for Packer provsioning.
