package org.bitbucket.kevstigneev.service_registry_h

import spock.lang.*
import groovy.util.logging.Slf4j

@Slf4j
class ImagePropertiesTest extends Specification {

    Properties projectProperties = mavenProperties

    def "Check VM image properties in the fully qualified file"() {
        log.info "Validate qualified VM image properties file"

        given: "Maven coordinates of the project"
            def groupId = projectProperties["project.groupId"]
            def artifactId = projectProperties["project.artifactId"]
            def version = projectProperties["project.version"]
            log.debug "The project coordrinates are ${groupId}:${artifactId}:${version}"
            def propertyPrefix = "${groupId}.${artifactId}"
        expect: "A file with the correct name"
            def propertiesFileName = propertyPrefix.tokenize(".").join("/") + "/image.properties"
            log.debug "The expected property file name is ${propertiesFileName}"
            ClassLoader.getSystemResourceAsStream(propertiesFileName)
        and: "It is in Java properties format"
            def imageProperties = new Properties()
            def resource = ClassLoader.getSystemResourceAsStream(propertiesFileName)
            imageProperties.load(resource)
            log.debug "Image properties: ${imageProperties}"
        and: "it contains correct VM image name"
            imageProperties["${propertyPrefix}.imageName"] == projectProperties["imageName"]
            // TODO: Test image project name
            log.warn "TODO: Image project is not validated"
        and: "The image name is a valid RFC1035 domain name label"
            def imageName = imageProperties["${propertyPrefix}.imageName"]
            imageName.length() <= 63
            imageName ==~ /[a-z].*/     // Starts at a lower case letter
            imageName ==~ /.*[a-z0-9]/  // Ends at a letter or a digit
            imageName ==~ /[a-z0-9-]*/  // Consists of letters, digits and hyphens
    }

    def "Check VM image properties in the global file"() {
        log.info "Validate global VM image properties file"

        expect: "A file with the correct name"
            def propertiesFileName = "image.properties"
            log.debug "The expected property file name is ${propertiesFileName}"
            ClassLoader.getSystemResourceAsStream(propertiesFileName)
        and: "It is in Java properties format"
            def imageProperties = new Properties()
            def resource = ClassLoader.getSystemResourceAsStream(propertiesFileName)
            imageProperties.load(resource)
            log.debug "Image properties: ${imageProperties}"
        and: "it contains correct VM image name"
            imageProperties["imageName"] == projectProperties["imageName"]
            // TODO: Test image project name
            log.warn "TODO: Image project is not validated"
    }

    private getMavenProperties() {
        def properties = new Properties()
        def resource = ClassLoader.getSystemResourceAsStream("project.properties")
        properties.load(resource)
        properties
    }

}
